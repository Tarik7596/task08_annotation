package com.babii;

public class MyOwnClass {
    @MyOwnAnnotation(year = 2018)
    private String model;

    @MyOwnAnnotation(year=2018)
    private int year;

    String trend;

    @MyOwnAnnotation(year = 2018,value = "Germany")
    private String producer;

    public MyOwnClass( int year, String trend, String producer )
    {

        this.trend=trend;
        this.year=year;
        this.producer=producer;

    }

    public String getModel()
    {
        return model;
    }
    public String getProducer()
    {
        return producer;
    }
    public String getTrend()
    {
        return trend;
    }
    public int getYear()
    {
        return year;
    }

    public String methodInvoke(String model, int year, boolean value)
    {
        return model;
    }

    public void MyMethod(String...arg){
        System.out.println("Hello");
    }

    public  void MyNewMethod(String st, int...arg){
        System.out.println("Bye");
    }
    @Override
    public String toString()
    {
        return producer+""+model+""+year+""+trend;
    }
}
