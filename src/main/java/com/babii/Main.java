package com.babii;

import java.lang.Class;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        MyOwnClass myOwnClass = new MyOwnClass(2015, "grey_color","Paris");
        UnknownClass<String> uC = new UnknownClass<String>();

        Class czz = Class.forName("com.babii.MyOwnClass");
        Class uCzz = uC.getClass();

        Field[] field = czz.getDeclaredFields();
        for (Field fl : field) {
            fl.setAccessible(true);
            MyOwnAnnotation myAnn = fl.getAnnotation(MyOwnAnnotation.class);

            if (fl.isAnnotationPresent(MyOwnAnnotation.class)) {
                System.out.println("Labaled field with annotation - " + fl.getName() + "; " + " annotation value-" + myAnn.value() + "; " + myAnn.year());

            }
        }


        Method mth =czz.getDeclaredMethod("methodInvoke", String.class, int.class, boolean.class);
        System.out.println("Invoke mothod- "+mth.invoke(myOwnClass, "model",2015,true)+";" +"Return type - "+mth.getReturnType());


        Field fl = czz.getDeclaredField("trend");
        fl.setAccessible(true);
        fl.set(myOwnClass, "color");
        System.out.println("Set value in field - "+ myOwnClass.trend);

        System.out.println("Invoke MyMethod(String...arg)");
        Method myMethod = czz.getDeclaredMethod("MyMethod", String[].class);
        String[] Args = new String[2];
        for (String s: Args)
            myMethod.invoke(myOwnClass , s);


        System.out.println("Invoke MyMethod(String st, int...arg)");
        Method myNewMethod = czz.getDeclaredMethod("MyNewMethod", String.class, int[].class);
        int [] arggg = new int [2] ;

        myNewMethod.invoke(myOwnClass ,"invoke", arggg);

        System.out.println("Information about generic Class");
        System.out.println(uCzz.getName()+" "+uCzz.getPackage()+" "+uCzz.getFields()+" "+ uCzz.getMethods());
    }
}
